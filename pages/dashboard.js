import {
  cvToHex,
  cvToValue,
  parseReadOnlyResponse,
  stringAsciiCV,
} from "@stacks/transactions";
import imageCompression from "browser-image-compression";
import FastAverageColor from "fast-average-color";
import { sha256 } from "js-sha256";
import Head from "next/head";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import { Spinner } from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";
import Modal from "react-bootstrap/Modal";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import { ReactSortable } from "react-sortablejs";
import { v4 as uuidv4 } from "uuid";
import { Constants } from "../common/constants";
import GetBtcDomain from "../components/dashboard/getBtcDomainPopup";
import NftPopup from "../components/dashboard/NftPopup";
import ProfilePreview from "../components/dashboard/profilePreview";
import SettingsPopup from "../components/dashboard/SettingsPopup";
import UserAccountDropdown from "../components/dashboard/userAccountDropdown";
import {
  getContractName,
  getContractOwner,
  getFileFromGaia,
  getMyStxAddress,
  getStacksAPIPrefix,
  getUserData,
  putFileToGaia,
  userSession,
} from "../services/auth";
import {
  mintOwlLinkProfile,
  republishOwlLinkProfile,
} from "../services/contract";
import { getNTFsMetaImage } from "../services/stacks-blockchain";
import {
  convertToPersonScheme,
  formStacksExplorerUrl,
  isEmail,
  isGaiaTokenExpired,
  isValidURL,
  openFacebookUrl,
  openLinkedinUrl,
  openRedditUrl,
  openTelegramUrl,
  openTwitterUrl,
  openWhatsappUrl,
  replaceOwlLinkIPFSPrefixForAllSavedImages,
} from "../services/utils";
import styles from "../styles/Dashboard.module.css";

// Load the full build.
const _ = require("lodash");

export default function Dashboard() {
  // Variables

  // OwlLink profile
  const [owlLinkProfile, setOwlLinkProfile] = useState();
  const handleChangeOwlLinkProfile = (e) => {
    const { name, value } = e.target;

    // If value is empty, then delete key from previous state
    if (!value && owlLinkProfile) {
      // Delete key from JSON
      delete owlLinkProfile[name];
    } else {
      // Update the value
      owlLinkProfile[name] = value;
    }
    setOwlLinkProfile({ ...owlLinkProfile });
  };

  // OwlLink previous profile
  const [previousOwlLinkProfile, setPreviousOwlLinkProfile] = useState();

  // BTC dns name
  const [dns, setDns] = useState();

  // Owl link minted or not flag
  const [mintOwlLinkProfileFlag, setMintOwlLinkProfileFlag] = useState(false);

  // Republish Owl link flag
  const [republishOwlLinkProfileFlag, setRepublishOwlLinkProfileFlag] =
    useState(false);

  // Is last transaction pending
  const [isLastTxPending, setIsLastTxPending] = useState(true);

  // GetBTCDomain popup
  const [showBTCDomainPopup, setShowBTCDomainPopup] = useState(false);

  // File upload - Profile image
  const hiddenAddProfileImageInput = useRef(null);
  const handleAddProfileClick = () => {
    if (isProfileImgUploading) {
      return;
    }

    hiddenAddProfileImageInput.current.click();
  };

  // Profile image uploading
  const [isProfileImgUploading, setIsProfileImgUploading] = useState(false);

  // Add new link popup
  const [showLinkPopup, setShowLinkPopup] = useState(false);
  const handleCloseLinkPopup = () => {
    setErrorMessage("");
    setShowLinkPopup(false);
  };
  const handleShowLinkPopup = () => {
    setLink({});
    setShowLinkPopup(true);
  };

  // Add social link popup
  const [showSocialLinkPopup, setShowSocialLinkPopup] = useState(false);
  const handleCloseSocialLinkPopup = () => {
    setErrorMessage("");
    setShowSocialLinkPopup(false);
  };
  const handleShowSocialLinkPopup = () => setShowSocialLinkPopup(true);

  // Add Nft link popup
  const [showNftLinkPopupFlag, setShowNftLinkPopupFlag] = useState(false);
  const handleShowNftLinkPopup = () => {
    getNftsImageObjFromGaia();
    setLink({ type: "nft", title: "My Bitcoin NFTs" });
    setShowNftLinkPopupFlag(true);
  };

  // New link
  const [link, setLink] = useState();
  const handleChangeLink = (isCheckbox, e) => {
    let fieldName;
    let fieldValue;

    // If input is from checkbox, then go inside
    if (isCheckbox) {
      const { name, checked } = e.target;
      fieldName = name;
      fieldValue = checked;
    } else {
      const { name, value } = e.target;
      fieldName = name;
      fieldValue = value;
    }

    // If value is empty, then delete key from previous state
    if (!fieldValue && link) {
      // Delete key from JSON
      delete link[fieldName];
    } else {
      // Update the value
      link[fieldName] = fieldValue;
    }
    setLink({ ...link });
  };

  // Link image uploading
  const [isLinkImgUploading, setIsLinkImgUploading] = useState(false);

  // Saving flag
  const [isSaving, setIsSaving] = useState(false);

  // Add transaction confirmation popup
  const [
    showTransactionConfirmationPopup,
    setShowTransactionConfirmationPopup,
  ] = useState(false);
  const handleCloseTransactionConfirmationPopup = () =>
    setShowTransactionConfirmationPopup(false);
  const handleShowTransactionConfirmationPopup = () =>
    setShowTransactionConfirmationPopup(true);

  // Transaction id
  const [transactionId, setTransactionId] = useState();

  // Error message
  const [errorMessage, setErrorMessage] = useState();

  // Copy text
  const [copyText, setCopyText] = useState("Copy");

  // My nft's
  const [nftsArray, setNftsArray] = useState();

  // My nft's image
  const [nftsImageObj, setNftsImageObj] = useState();

  // My nft's count
  const [nftsTotal, setNftsTotal] = useState(0);

  // My nft's pagination active page number
  const [nftsPaginationActivePage, setNftsPaginationActivePage] = useState(1);

  // Show Settings popup flag
  const [showSettingsPopupFlag, setShowSettingsPopupFlag] = useState(false);

  // OwlLink profile settings
  const [owlLinkSettings, setOwlLinkSettings] = useState();

  // Previous settings state
  const [previousOwlLinkSettings, setPreviousOwlLinkSettings] = useState();

  /**
   * After view render
   */
  useEffect(() => {
    /**
     * Get BTC domain from blockchain
     */
    const getBTCDomainFromBlockchain = async () => {
      // Get btc domain for logged in user
      const response = await fetch(
        getStacksAPIPrefix() + "/v1/addresses/stacks/" + getMyStxAddress()
      );
      const responseObject = await response.json();

      // Testnet code
      if (Constants.STACKS_MAINNET_FLAG == false) {
        const _dns = getMyStxAddress().substr(-5) + ".btc";
        setDns(_dns);
        getOwlLinkProfileFromBlockchain(_dns);
        return;
      }

      // Get btc dns
      if (responseObject?.names?.length > 0) {
        const btcDNS = responseObject.names.filter((bns) =>
          bns.endsWith(".btc")
        );

        // Check does BTC dns is available
        if (btcDNS && btcDNS.length > 0) {
          const _dns = btcDNS[0];

          // Take the btc dns name
          setDns(_dns);

          // Get owl link profile from blockchain
          getOwlLinkProfileFromBlockchain(_dns);
        } else {
          // Show to buy BTC dns
          setShowBTCDomainPopup(true);
        }
      } else {
        // Show to buy BTC dns
        setShowBTCDomainPopup(true);
      }
    };

    /**
     * Get owl link profile from user's gaia
     */
    const getOwlLinkProfileFromGaia = () => {
      // Get OwlLink profile
      getFileFromGaia("owllink.json", { decrypt: false }).then(
        (response) => {
          if (response) {
            // Convert to Person scheme
            const responseJSON = JSON.parse(response);
            convertToPersonScheme(responseJSON);

            // Update OwlLink profile
            owlLinkProfile = responseJSON;
            setOwlLinkProfile(owlLinkProfile);

            // Store previous owl link profile as string
            setPreviousOwlLinkProfile(
              JSON.parse(JSON.stringify(owlLinkProfile))
            );
          } else {
            // Create new OwlLink profile
            createOwlLinkProfile();
          }
        },
        (error) => {
          // File does not exit in gaia
          if (error && error.code == "does_not_exist") {
            // Create new OwlLink profile
            createOwlLinkProfile();

            // Set empty object to save
            owlLinkProfile = {};

            // Save Empty OwlLink profile
            saveOwlLinkProfile();

            // Send signup event to fathom analytics
            if (window && window.fathom) {
              window.fathom.trackGoal("8S9SGSH0", 0);
            }
          }
        }
      );
    };

    // Check for login
    if (!userSession.isUserSignedIn()) {
      window.location.assign("/");
      return;
    }

    // Gaia token validity check
    isGaiaTokenExpired();

    // Get btc domain from blockchain
    getBTCDomainFromBlockchain();

    // Get owl link profile from gaia
    getOwlLinkProfileFromGaia();

    // Get last transaction status
    getLastTxStatus();

    // Load help scout
    !(function (e, t, n) {
      function a() {
        var e = t.getElementsByTagName("script")[0],
          n = t.createElement("script");
        (n.type = "text/javascript"),
          (n.async = !0),
          (n.src = "https://beacon-v2.helpscout.net"),
          e.parentNode.insertBefore(n, e);
      }
      if (
        ((e.Beacon = n =
          function (t, n, a) {
            e.Beacon.readyQueue.push({ method: t, options: n, data: a });
          }),
        (n.readyQueue = []),
        "complete" === t.readyState)
      )
        return a();
      e.attachEvent
        ? e.attachEvent("onload", a)
        : e.addEventListener("load", a, !1);
    })(window, document, window.Beacon || function () {});
    window.Beacon("init", "f5a98c28-4e98-481f-b059-e8334d53f0a4");
  }, []);

  /**
   * Get owl link profile from blockchain
   */
  const getOwlLinkProfileFromBlockchain = async (dns) => {
    if (dns) {
      let url =
        getStacksAPIPrefix() +
        "/v2/contracts/call-read/" +
        getContractOwner() +
        "/" +
        getContractName() +
        "/get-token-uri";

      // Fetch gaia URL from stacks blockchain
      const rawResponse = await fetch(url, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          sender: getContractOwner(),
          arguments: [cvToHex(stringAsciiCV(dns))],
        }),
      });
      const content = await rawResponse.json();

      // If data found on stacks blockchain
      if (content && content.okay) {
        // Get gaia address from blockchain
        const gaiaUrlFromBlockchain = cvToValue(parseReadOnlyResponse(content))
          .value.value;

        // Form gaia url
        let gaiaUrlPrefix = getUserData().gaiaHubConfig?.url_prefix;
        let gaiaAddress = getUserData().gaiaHubConfig?.address;
        let gaiaUrlFromCurrentLogin =
          gaiaUrlPrefix + gaiaAddress + "/owllink.json";

        // If gaiaUrl is empty, show button to update gaia URL
        if (gaiaUrlFromBlockchain != gaiaUrlFromCurrentLogin) {
          // Call contract function to update gaia URL
          setRepublishOwlLinkProfileFlag(true);
        }
      } else {
        // If data found on stacks blockchain
        setMintOwlLinkProfileFlag(true);
      }
    }
  };

  /**
   * Get last transaction status
   */
  const getLastTxStatus = () => {
    // Store transaction to Gaia
    getFileFromGaia("transactions.json").then(
      (response) => {
        if (response) {
          const transactionsObj = JSON.parse(response);

          if (transactionsObj && transactionsObj.transactions) {
            let lastTx =
              transactionsObj.transactions[
                transactionsObj.transactions.length - 1
              ];
            if (lastTx && lastTx.txId) {
              let url = getStacksAPIPrefix() + "/extended/v1/tx/" + lastTx.txId;
              fetch(url).then(async (rawResponse) => {
                let responseObj = await rawResponse.json();

                if (responseObj.tx_status != "pending") {
                  // Set last transaction is not pending
                  setIsLastTxPending(false);
                }
              });
            } else {
              // Set last transaction is not pending
              setIsLastTxPending(false);
            }
          } else {
            // Set last transaction is not pending
            setIsLastTxPending(false);
          }
        } else {
          // Set last transaction is not pending
          setIsLastTxPending(false);
        }
      },
      (error) => {
        // File does not exit in gaia
        if (error && error.code == "does_not_exist") {
          // Set last transaction is not pending
          setIsLastTxPending(false);
        }
      }
    );
  };

  /**
   * Create new owl link profile
   */
  const createOwlLinkProfile = () => {
    const newOwlLinkProfile = {};

    // Store previous owl link profile as string
    setPreviousOwlLinkProfile(JSON.parse(JSON.stringify(newOwlLinkProfile)));

    // Create new OwlLink profile
    setOwlLinkProfile(newOwlLinkProfile);
  };

  /**
   * On Profile image upload
   *
   * @param {*} event
   */
  const profileImageOnChange = () => {
    if (hiddenAddProfileImageInput?.current?.files) {
      imageOnChange(hiddenAddProfileImageInput, "profile");
    }
  };

  /**
   * Remove profile image
   */
  const removeProfileImage = () => {
    // Update the state
    handleChangeOwlLinkProfile({
      target: {
        name: "image",
        value: "",
      },
    });

    // Update the state
    handleChangeOwlLinkProfile({
      target: {
        name: "imageColor",
        value: "",
      },
    });
  };

  /**
   * On Image upload of Profile or Link
   *
   * @param {*} imageInput
   */
  const imageOnChange = (imageInput, type) => {
    if (imageInput?.current?.files) {
      const files = imageInput.current.files;

      if (files?.item(0)) {
        const imageFile = files.item(0);
        try {
          // Supported format
          if (
            [
              "image/jpg",
              "image/jpeg",
              "image/png",
              "image/svg+xml",
              "image/webp",
              "image/gif",
            ].includes(imageFile.type)
          ) {
            // Start uploading
            if (type == "profile") {
              setIsProfileImgUploading(true);
            } else if (type == "link") {
              setIsLinkImgUploading(true);
            }

            // Compress only when the size is more than 500 kb
            if (imageFile.type !== "image/gif" && imageFile.size > 200000) {
              let options = {
                maxSizeMB: 0.2,
                maxWidthOrHeight: 1024,
                useWebWorker: true,
              };

              // Compress image
              imageCompression(imageFile, options)
                .then((compressedFile) => {
                  // Upload the compressed image
                  uploadImage(compressedFile, type);
                })
                .catch((error) => {
                  throw error.message;
                });
            } else {
              // Upload directly
              uploadImage(imageFile, type);
            }
          } else {
            throw "The file type you uploaded isn't supported. Try again with a JPG, PNG, or GIF.";
          }
        } catch (e) {
          console.error(e);
        }
      }
    }
  };

  /**
   * upload image
   *
   * @param imageFile
   */
  const uploadImage = (imageFile, type) => {
    const reader = new FileReader();
    reader.onload = imageReadAsDataURLCallBack.bind(this, imageFile, type);
    reader.readAsDataURL(imageFile);
  };

  /**
   * Call back function for file conversion to base64
   *
   * @param readerEvt
   */
  const imageReadAsDataURLCallBack = (imageFile, type, readerEvt) => {
    const base64String = readerEvt.target.result;

    // Get unique id
    const hashedCode = sha256(base64String);

    // File format
    let fileName = imageFile.name;
    let fileNameSplitted = fileName.split(".");
    if (fileNameSplitted.length > 0) {
      // File name with image hash-code
      fileName =
        hashedCode + "." + fileNameSplitted[fileNameSplitted.length - 1];
    }

    // Publish with readable for everyone
    const options = {
      encrypt: false,
      dangerouslyIgnoreEtag: true,
      contentType: imageFile.type,
    };

    // Store image file on gaia
    putFileToGaia(fileName, imageFile, options).then(async (response) => {
      if (type == "profile") {
        // Get background color
        const fac = new FastAverageColor();
        const color = await fac.getColorAsync(response);

        // Update the state
        handleChangeOwlLinkProfile({
          target: {
            name: "image",
            value: response,
          },
        });

        // Update the state for image background color
        handleChangeOwlLinkProfile({
          target: {
            name: "imageColor",
            value: color.rgba,
          },
        });

        // Stop uploading
        setIsProfileImgUploading(false);
      } else if (type == "link") {
        // Update the state
        handleChangeLink(false, {
          target: {
            name: "img",
            value: response,
          },
        });

        // Stop uploading
        setIsLinkImgUploading(false);
      }
    });
  };

  /**
   * Add new link to profile
   */
  const addLink = () => {
    if (link?.title) {
      let newLink;

      // Nft link
      if (link.type == "nft") {
        // New NFT link

        // Create new NFT link
        newLink = {
          id: uuidv4(),
          type: "nft",
          title: link.title,
          img: link.img ? link.img : "",
          nfts: link.nfts ? link.nfts : "",
        };
      } else {
        // New link

        // Validation
        if (link.url && !isValidURL(link.url) && !isEmail(link.url)) {
          // Show error
          setErrorMessage("Enter valid url");
          return;
        }

        // Create new link
        newLink = {
          id: uuidv4(),
          title: link.title,
          url: link.url,
          img: link.img ? link.img : "",
          embed: link.embed ? link.embed : "",
        };
      }

      let links = owlLinkProfile.links || [];

      // Push to array of links
      links.push(newLink);

      // Update the state
      handleChangeOwlLinkProfile({
        target: {
          name: "links",
          value: links,
        },
      });

      // Close new link popup
      handleCloseLinkPopup();

      // Close new Nft link popup
      setShowNftLinkPopupFlag(false);
    }
  };

  /**
   * Edit link
   */
  const editLink = (link) => {
    // Update link object
    setLink({ ...link });

    if (link?.type === "nft") {
      // Fetch all nfts meta image
      getNftsImageObjFromGaia();

      // Show nft link popup
      setShowNftLinkPopupFlag(true);
    } else {
      // Show link popup to edit or delete
      setShowLinkPopup(true);
    }
  };

  /**
   * Save edit link
   */
  const saveLink = () => {
    if (link?.id) {
      // Nft link
      if (link.type !== "nft") {
        // Link

        // Validate url
        if (link.url && !isValidURL(link.url) && !isEmail(link.url)) {
          // Show error
          setErrorMessage("Enter valid url");
          return;
        }
      }

      let linkIndex = owlLinkProfile.links.findIndex(
        (eachLink) => eachLink.id == link.id
      );

      // Update link
      owlLinkProfile.links[linkIndex] = link;

      // Update the state
      handleChangeOwlLinkProfile({
        target: {
          name: "links",
          value: owlLinkProfile.links,
        },
      });

      // Close new link popup
      handleCloseLinkPopup();
    }
  };

  /**
   * Delete link
   */
  const deleteLink = () => {
    if (link && link.id) {
      let linkIndex = owlLinkProfile.links.findIndex(
        (eachLink) => eachLink.id == link.id
      );

      // Delete link
      owlLinkProfile.links.splice(linkIndex, 1);

      // Update the state
      handleChangeOwlLinkProfile({
        target: {
          name: "links",
          value: owlLinkProfile.links,
        },
      });

      // Close new link popup
      handleCloseLinkPopup();
    }
  };

  /**
   * Save owl link profile to gaia
   */
  const saveOwlLinkProfile = () => {
    // Start loading
    setIsSaving(true);

    // Convert to person schema
    convertToPersonScheme(owlLinkProfile);

    // Take cut from current state
    const savedOwlLinkProfile = JSON.parse(JSON.stringify(owlLinkProfile));

    // Save owlLink profile
    putFileToGaia("owllink.json", JSON.stringify(savedOwlLinkProfile), {
      encrypt: false,
      dangerouslyIgnoreEtag: true,
    }).then(() => {
      // Store previous owl link profile as string
      setPreviousOwlLinkProfile(savedOwlLinkProfile);

      // Stop loading
      setIsSaving(false);
    });
  };

  /**
   * Copy public path to clip board
   */
  const copyToClipBoard = () => {
    if (dns) {
      // Update tooltip message
      setCopyText("Copied");

      let publicUrl = "https://owl.link/" + dns;
      navigator.clipboard.writeText(publicUrl);
    }
  };

  // Social links - On change
  const [socialLinks, setSocialLinks] = useState({});
  const handleChangeSocialLink = (e) => {
    const { name, value } = e.target;

    // If value is empty, then delete key from previous state
    if (!value && socialLinks) {
      // Delete key from JSON
      delete socialLinks[name];
    } else {
      // Update the value
      socialLinks[name] = value;
    }

    setSocialLinks({ ...socialLinks });
  };

  // Edit social links
  const editSocialLinks = () => {
    // Get previous social links to edit
    if (owlLinkProfile && owlLinkProfile.socialLinks) {
      setSocialLinks({ ...owlLinkProfile.socialLinks });
    } else {
      // Empty object
      setSocialLinks({ ...{} });
    }

    // Show social links popup
    handleShowSocialLinkPopup();
  };

  // Save social links
  const saveSocialLinks = () => {
    if (socialLinks && Object.keys(socialLinks).length > 0) {
      // Validate url
      for (let key in socialLinks) {
        if (key == "email") {
          if (!isEmail(socialLinks[key])) {
            // Show error
            setErrorMessage("Enter valid email");
            return;
          }
        } else {
          if (!isValidURL(socialLinks[key])) {
            // Show error
            setErrorMessage("Enter valid url");
            return;
          }
        }
      }
    }

    // Update link
    owlLinkProfile.socialLinks = socialLinks;

    // Update the state
    handleChangeOwlLinkProfile({
      target: {
        name: "socialLinks",
        value: owlLinkProfile.socialLinks,
      },
    });

    // Close social link popup
    handleCloseSocialLinkPopup();
  };

  /**
   * On successful mint
   *
   * @param {*} data
   */
  const mintCallbackFunction = (data) => {
    if (data.txId) {
      // Set transaction Id
      setTransactionId(data.txId);

      // Set last transaction is pending
      setIsLastTxPending(true);

      // Store transaction to Gaia
      getFileFromGaia("transactions.json").then(
        (response) => {
          if (response) {
            const transactionsObj = JSON.parse(response);

            if (transactionsObj && transactionsObj.transactions) {
              transactionsObj.transactions.push({
                txId: data.txId,
                txRaw: data.txRaw,
                date: Date.now(),
              });

              // Store on gaia
              putFileToGaia(
                "transactions.json",
                JSON.stringify(transactionsObj),
                { dangerouslyIgnoreEtag: true }
              );
            }
          }
        },
        (error) => {
          // File does not exit in gaia
          if (error && error.code == "does_not_exist") {
            const transactionsObj = {
              transactions: [
                {
                  txId: data.txId,
                  txRaw: data.txRaw,
                  date: Date.now(),
                },
              ],
            };

            // Store on gaia
            putFileToGaia(
              "transactions.json",
              JSON.stringify(transactionsObj),
              { dangerouslyIgnoreEtag: true }
            );
          }
        }
      );

      // Show information popup
      handleShowTransactionConfirmationPopup();
    }
  };

  /**
   * Drag and Drop
   */
  const dragAndDropLinks = (newState) => {
    owlLinkProfile.links = newState;

    // Update the state
    handleChangeOwlLinkProfile({
      target: {
        name: "links",
        value: owlLinkProfile.links,
      },
    });
  };

  /**
   * Download nft's image from gaia
   */
  const getNftsImageObjFromGaia = (pageNumber = 1) => {
    // Reset always - Nfts array
    setNftsArray();

    // Set page number
    setNftsPaginationActivePage(pageNumber);

    // Calculate offset
    const offset = 50 * (pageNumber - 1);

    // If nftsImageObj already fetched from gaia, then proceed directly
    if (nftsImageObj) {
      // Fetch new nft images
      getNTFsMetaImage(
        setNftsArray,
        nftsImageObj,
        setNftsImageObj,
        saveNftsImageObjToGaia,
        setNftsTotal,
        offset
      );
    } else {
      // Get NFT's images from gaia
      getFileFromGaia("nftsImage.json", { decrypt: false }).then(
        (response) => {
          if (response) {
            const responseObj = JSON.parse(response);

            // Replace with owl.link pinata cloud URL
            replaceOwlLinkIPFSPrefixForAllSavedImages(responseObj);

            // Update existing data
            nftsImageObj = { ...responseObj };
            setNftsImageObj(nftsImageObj);

            // Fetch new nft images
            getNTFsMetaImage(
              setNftsArray,
              nftsImageObj,
              setNftsImageObj,
              saveNftsImageObjToGaia,
              setNftsTotal,
              offset
            );
          } else {
            nftsImageObj = {};
            setNftsImageObj(nftsImageObj);

            // Fetch new nft images
            getNTFsMetaImage(
              setNftsArray,
              nftsImageObj,
              setNftsImageObj,
              saveNftsImageObjToGaia,
              setNftsTotal,
              offset
            );
          }
        },
        (error) => {
          // File does not exit in gaia
          if (error && error.code == "does_not_exist") {
            nftsImageObj = {};
            setNftsImageObj(nftsImageObj);

            // Fetch new nft images
            getNTFsMetaImage(
              setNftsArray,
              nftsImageObj,
              setNftsImageObj,
              saveNftsImageObjToGaia,
              setNftsTotal,
              offset
            );
          }
        }
      );
    }
  };

  /**
   * Save nft's images to gaia
   */
  const saveNftsImageObjProcessing = false;
  const saveNftsImageObjToGaia = () => {
    if (saveNftsImageObjProcessing == true) {
      return;
    }

    // Start processing
    saveNftsImageObjProcessing = true;

    // Publish with readable for everyone
    const options = {
      encrypt: false,
      dangerouslyIgnoreEtag: true,
    };

    const nftsImageObjString = JSON.stringify(nftsImageObj);
    // Store image file on gaia
    putFileToGaia("nftsImage.json", nftsImageObjString, options).then(() => {
      // Stop processing
      saveNftsImageObjProcessing = false;

      // If there is a difference between saved and new object, then save again
      if (JSON.stringify(nftsImageObj) != nftsImageObjString) {
        // Save again
        saveNftsImageObjToGaia();
      }
    });
  };

  /**
   * Show settings popup
   */
  const showSettingsPopup = () => {
    let cloneOwlLinkSettings = {
      ...(owlLinkProfile.settings ? owlLinkProfile.settings : {}),
    };

    // Default value
    if (
      cloneOwlLinkSettings.showOwlLinkLogo !== true &&
      cloneOwlLinkSettings.showOwlLinkLogo !== false
    ) {
      cloneOwlLinkSettings.showOwlLinkLogo = true;
    }

    // Clone settings object
    setOwlLinkSettings(cloneOwlLinkSettings);

    // Take previous owl link settings
    setPreviousOwlLinkSettings({ ...cloneOwlLinkSettings });

    // Open popup
    setShowSettingsPopupFlag(true);
  };

  return (
    <>
      {/* Header */}
      <Head>
        <title>Dashboard | Owl Link</title>
        <meta
          name="description"
          content="Showcase your collectibles, NFTs, and social profiles. Everything is in one place."
        />
      </Head>

      {/* Body */}
      <main>
        <section>
          <div
            className="container-fluid"
            style={{ height: "100vh", overflow: "hidden" }}
          >
            <div className="row justify-content-center h-100">
              <div className="col-md-12" style={{ padding: "0px" }}>
                <div className={styles.dashboard_outline_box}>
                  {/* Left side */}
                  <div className={styles.dashboard_left_box}>
                    {/* Logo and .btc url */}
                    <div className={styles.dashboard_left_nav_box}>
                      {/* Logo */}
                      <Link href="/">
                        <svg
                          width="34"
                          height="47"
                          viewBox="0 0 34 47"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                          style={{ cursor: "pointer" }}
                        >
                          <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M28.4399 46.0283H25.5764C11.4508 46.0283 0 34.5775 0 20.4519V14.8314C0 6.64035 6.64035 0 14.8313 0H19.1682C27.3592 0 33.9996 6.64035 33.9996 14.8314V21.0058C33.9996 21.3919 33.8523 21.7627 33.5884 22.045L33.5355 22.1016C33.3616 22.2877 33.1975 22.4635 33.1598 22.4802C32.3007 22.8258 31.9838 23.0366 31.3124 23.4832C31.2484 23.5258 31.1812 23.5705 31.11 23.6177C27.126 26.2668 24.6661 30.7316 24.6661 35.6668C24.6661 38.3467 25.4127 40.9192 26.7476 43.1388C27.28 44.023 27.9065 44.85 28.6173 45.6044C28.7692 45.7655 28.6608 46.0283 28.4399 46.0283ZM9.52275 7.91687C6.59739 8.12144 4.17835 10.5763 4.01981 13.5068C3.84592 16.7748 6.35702 19.5825 9.61992 19.7615C9.67106 19.7666 9.72221 19.7717 9.76823 19.782H13.2664C14.3404 19.782 15.3479 20.2013 16.1099 20.9582L16.9998 21.8481L17.8897 20.9582C18.6517 20.2013 19.6592 19.782 20.7332 19.782H24.5996C24.6814 19.7513 24.7632 19.7308 24.8502 19.7206C27.9545 19.3012 30.2048 16.5139 29.9696 13.3789C29.7496 10.4689 27.3869 8.12144 24.4768 7.91687C21.2855 7.69185 18.4983 10.0035 18.1505 13.1794C18.084 13.7676 17.5879 14.2074 16.9998 14.2074C16.4117 14.2074 15.9156 13.7676 15.8491 13.1794C15.5013 10.0086 12.7192 7.70208 9.52275 7.91687ZM31.112 27.2969C31.8914 26.5512 32.7797 25.9073 33.7637 25.3938C33.8701 25.3381 34 25.4179 34 25.5375V46.0289H33.8098C33.7213 45.9434 33.6251 45.8642 33.5126 45.8018C32.2652 45.1078 31.1836 44.199 30.2917 43.1413C28.5543 41.0808 27.5519 38.4459 27.5519 35.6668C27.5519 32.4259 28.8755 29.4367 31.112 27.2969ZM10.0633 16.2915C11.4163 16.2915 12.5131 15.1947 12.5131 13.8417C12.5131 12.4888 11.4163 11.392 10.0633 11.392C8.71038 11.392 7.6136 12.4888 7.6136 13.8417C7.6136 15.1947 8.71038 16.2915 10.0633 16.2915ZM26.3853 13.8417C26.3853 15.1947 25.2886 16.2915 23.9356 16.2915C22.5827 16.2915 21.4859 15.1947 21.4859 13.8417C21.4859 12.4888 22.5827 11.392 23.9356 11.392C25.2886 11.392 26.3853 12.4888 26.3853 13.8417Z"
                            fill="url(#paint0_linear_47_132)"
                          />
                          <defs>
                            <linearGradient
                              id="paint0_linear_47_132"
                              x1="23.9355"
                              y1="11.392"
                              x2="23.9355"
                              y2="16.2915"
                              gradientUnits="userSpaceOnUse"
                            >
                              <stop stopColor="#FF793F" />
                              <stop offset="1" stopColor="#FE6844" />
                            </linearGradient>
                          </defs>
                        </svg>
                      </Link>

                      {/* Link and profile dropdown */}
                      <div
                        className="d-flex align-items-center justify-content-center"
                        style={{ columnGap: "15px" }}
                      >
                        {dns && (
                          <div
                            className="d-flex align-items-center justify-content-center"
                            style={{ columnGap: "10px" }}
                          >
                            {/* Btc url copy button */}
                            <a
                              className="d-none d-md-block"
                              style={{
                                textDecoration: "underline",
                                color: "#ff793f",
                                fontSize: "16px",
                                fontWeight: "500",
                              }}
                              href={"/" + dns}
                              target="_blank"
                              rel="noreferrer"
                            >
                              {"owl.link/" + dns}
                            </a>

                            {/* Copy icon */}
                            <OverlayTrigger
                              placement="auto"
                              overlay={<Tooltip>{copyText}</Tooltip>}
                            >
                              <button
                                className={styles.dashboard_left_btc_url_btn}
                                onClick={copyToClipBoard}
                                onMouseEnter={() => setCopyText("Copy")}
                              >
                                <span>
                                  <svg
                                    className={
                                      styles.dashboard_btc_url_copy_icon
                                    }
                                    viewBox="0 0 18 18"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      d="M16.4203 0H4.6831C3.81103 0 3.10345 0.707586 3.10345 1.57966V3.10345H1.57966C0.707586 3.10345 0 3.81103 0 4.6831V16.4203C0 17.2924 0.707586 18 1.57966 18H13.3169C14.189 18 14.8966 17.2924 14.8966 16.4203V14.8966H16.4203C17.2924 14.8966 18 14.189 18 13.3169V1.57966C18 0.707586 17.2924 0 16.4203 0ZM13.6552 16.4203C13.6552 16.6066 13.5031 16.7586 13.3169 16.7586H1.57966C1.39345 16.7586 1.24138 16.6066 1.24138 16.4203V4.6831C1.24138 4.4969 1.39345 4.34483 1.57966 4.34483H13.3169C13.5031 4.34483 13.6552 4.4969 13.6552 4.6831V16.4203ZM16.7586 13.3169C16.7586 13.5031 16.6066 13.6552 16.4203 13.6552H14.8966V4.6831C14.8966 3.81103 14.189 3.10345 13.3169 3.10345H4.34483V1.57966C4.34483 1.39345 4.4969 1.24138 4.6831 1.24138H16.4203C16.6066 1.24138 16.7586 1.39345 16.7586 1.57966V13.3169Z"
                                      fill="#FF793F"
                                    />
                                  </svg>
                                </span>
                              </button>
                            </OverlayTrigger>

                            {/* Share icon */}
                            <Dropdown>
                              <Dropdown.Toggle
                                id="user-account-dropdown"
                                className={styles.dashboard_left_btc_url_btn}
                                variant="light"
                              >
                                <OverlayTrigger
                                  placement="auto"
                                  overlay={<Tooltip>Share</Tooltip>}
                                >
                                  <div className={styles.dashboard_share_icon}>
                                    <svg
                                      className={
                                        styles.dashboard_btc_url_copy_icon
                                      }
                                      viewBox="0 0 16 16"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                    >
                                      <path
                                        d="M12.3845 9.9624C11.4516 9.9624 10.6163 10.388 10.0622 11.0551L6.45721 9.00838C6.56931 8.69291 6.63093 8.35371 6.63093 8.00021C6.63093 7.64679 6.56931 7.30758 6.45721 6.99211L10.0619 4.94486C10.616 5.61221 11.4514 6.03796 12.3844 6.03796C14.0486 6.03796 15.4026 4.68347 15.4026 3.01852C15.4028 1.35406 14.0488 0 12.3845 0C10.7201 0 9.36594 1.35406 9.36594 3.01845C9.36594 3.37195 9.42749 3.71122 9.53966 4.02676L5.93478 6.07408C5.38066 5.40708 4.54551 4.98162 3.61276 4.98162C1.94809 4.98162 0.59375 6.33568 0.59375 8.00014C0.59375 9.66453 1.94809 11.0186 3.61276 11.0186C4.54551 11.0186 5.38059 10.5931 5.93471 9.9262L9.53966 11.973C9.42749 12.2886 9.36587 12.6279 9.36587 12.9815C9.36587 14.6459 10.72 15.9999 12.3845 15.9999C14.0487 15.9999 15.4027 14.6458 15.4027 12.9815C15.4028 11.3168 14.0488 9.9624 12.3845 9.9624ZM12.3845 1.05626C13.4664 1.05626 14.3465 1.93648 14.3465 3.01845C14.3465 4.10098 13.4664 4.98162 12.3845 4.98162C11.3025 4.98162 10.4222 4.10098 10.4222 3.01845C10.4222 1.93648 11.3025 1.05626 12.3845 1.05626ZM3.61283 9.9624C2.53058 9.9624 1.65008 9.08211 1.65008 8.00021C1.65008 6.91817 2.53058 6.03796 3.61283 6.03796C4.69466 6.03796 5.57473 6.91817 5.57473 8.00021C5.57473 9.08211 4.69459 9.9624 3.61283 9.9624ZM12.3845 14.9437C11.3025 14.9437 10.4222 14.0634 10.4222 12.9816C10.4222 11.8992 11.3025 11.0187 12.3845 11.0187C13.4664 11.0187 14.3465 11.8992 14.3465 12.9816C14.3465 14.0634 13.4664 14.9437 12.3845 14.9437Z"
                                        fill="#FF793F"
                                      />
                                    </svg>
                                  </div>
                                </OverlayTrigger>
                              </Dropdown.Toggle>

                              <Dropdown.Menu>
                                <Dropdown.Item
                                  onClick={() => {
                                    openTwitterUrl(
                                      "https://owl.link/" + dns,
                                      owlLinkProfile && owlLinkProfile.name
                                        ? owlLinkProfile.name
                                        : dns
                                    );
                                  }}
                                >
                                  Twitter
                                </Dropdown.Item>
                                <Dropdown.Item
                                  onClick={() => {
                                    openFacebookUrl(
                                      "https://owl.link/" + dns,
                                      owlLinkProfile && owlLinkProfile.name
                                        ? owlLinkProfile.name
                                        : dns
                                    );
                                  }}
                                >
                                  Facebook
                                </Dropdown.Item>
                                <Dropdown.Item
                                  onClick={() => {
                                    openLinkedinUrl(
                                      "https://owl.link/" + dns,
                                      owlLinkProfile && owlLinkProfile.name
                                        ? owlLinkProfile.name
                                        : dns
                                    );
                                  }}
                                >
                                  LinkedIn
                                </Dropdown.Item>
                                <Dropdown.Item
                                  onClick={() => {
                                    openWhatsappUrl(
                                      "https://owl.link/" + dns,
                                      owlLinkProfile && owlLinkProfile.name
                                        ? owlLinkProfile.name
                                        : dns
                                    );
                                  }}
                                >
                                  WhatsApp
                                </Dropdown.Item>
                                <Dropdown.Item
                                  onClick={() => {
                                    openTelegramUrl(
                                      "https://owl.link/" + dns,
                                      owlLinkProfile && owlLinkProfile.name
                                        ? owlLinkProfile.name
                                        : dns
                                    );
                                  }}
                                >
                                  Telegram
                                </Dropdown.Item>
                                <Dropdown.Item
                                  onClick={() => {
                                    openRedditUrl(
                                      "https://owl.link/" + dns,
                                      owlLinkProfile && owlLinkProfile.name
                                        ? owlLinkProfile.name
                                        : dns
                                    );
                                  }}
                                >
                                  Reddit
                                </Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>

                            {/* Settings */}
                            <OverlayTrigger
                              placement="auto"
                              overlay={<Tooltip>Settings</Tooltip>}
                            >
                              <button
                                className={styles.dashboard_left_btc_url_btn}
                                onClick={() => {
                                  showSettingsPopup();
                                }}
                              >
                                <span>
                                  <svg
                                    className={
                                      styles.dashboard_btc_url_copy_icon
                                    }
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 478.703 478.703"
                                  >
                                    <path
                                      d="M454.2,189.101l-33.6-5.7c-3.5-11.3-8-22.2-13.5-32.6l19.8-27.7c8.4-11.8,7.1-27.9-3.2-38.1l-29.8-29.8 c-5.6-5.6-13-8.7-20.9-8.7c-6.2,0-12.1,1.9-17.1,5.5l-27.8,19.8c-10.8-5.7-22.1-10.4-33.8-13.9l-5.6-33.2 c-2.4-14.3-14.7-24.7-29.2-24.7h-42.1c-14.5,0-26.8,10.4-29.2,24.7l-5.8,34c-11.2,3.5-22.1,8.1-32.5,13.7l-27.5-19.8 c-5-3.6-11-5.5-17.2-5.5c-7.9,0-15.4,3.1-20.9,8.7l-29.9,29.8c-10.2,10.2-11.6,26.3-3.2,38.1l20,28.1 c-5.5,10.5-9.9,21.4-13.3,32.7l-33.2,5.6c-14.3,2.4-24.7,14.7-24.7,29.2v42.1c0,14.5,10.4,26.8,24.7,29.2l34,5.8 c3.5,11.2,8.1,22.1,13.7,32.5l-19.7,27.4c-8.4,11.8-7.1,27.9,3.2,38.1l29.8,29.8c5.6,5.6,13,8.7,20.9,8.7c6.2,0,12.1-1.9,17.1-5.5 l28.1-20c10.1,5.3,20.7,9.6,31.6,13l5.6,33.6c2.4,14.3,14.7,24.7,29.2,24.7h42.2c14.5,0,26.8-10.4,29.2-24.7l5.7-33.6 c11.3-3.5,22.2-8,32.6-13.5l27.7,19.8c5,3.6,11,5.5,17.2,5.5l0,0c7.9,0,15.3-3.1,20.9-8.7l29.8-29.8c10.2-10.2,11.6-26.3,3.2-38.1 l-19.8-27.8c5.5-10.5,10.1-21.4,13.5-32.6l33.6-5.6c14.3-2.4,24.7-14.7,24.7-29.2v-42.1 C478.9,203.801,468.5,191.501,454.2,189.101z M451.9,260.401c0,1.3-0.9,2.4-2.2,2.6l-42,7c-5.3,0.9-9.5,4.8-10.8,9.9 c-3.8,14.7-9.6,28.8-17.4,41.9c-2.7,4.6-2.5,10.3,0.6,14.7l24.7,34.8c0.7,1,0.6,2.5-0.3,3.4l-29.8,29.8c-0.7,0.7-1.4,0.8-1.9,0.8 c-0.6,0-1.1-0.2-1.5-0.5l-34.7-24.7c-4.3-3.1-10.1-3.3-14.7-0.6c-13.1,7.8-27.2,13.6-41.9,17.4c-5.2,1.3-9.1,5.6-9.9,10.8l-7.1,42 c-0.2,1.3-1.3,2.2-2.6,2.2h-42.1c-1.3,0-2.4-0.9-2.6-2.2l-7-42c-0.9-5.3-4.8-9.5-9.9-10.8c-14.3-3.7-28.1-9.4-41-16.8 c-2.1-1.2-4.5-1.8-6.8-1.8c-2.7,0-5.5,0.8-7.8,2.5l-35,24.9c-0.5,0.3-1,0.5-1.5,0.5c-0.4,0-1.2-0.1-1.9-0.8l-29.8-29.8 c-0.9-0.9-1-2.3-0.3-3.4l24.6-34.5c3.1-4.4,3.3-10.2,0.6-14.8c-7.8-13-13.8-27.1-17.6-41.8c-1.4-5.1-5.6-9-10.8-9.9l-42.3-7.2 c-1.3-0.2-2.2-1.3-2.2-2.6v-42.1c0-1.3,0.9-2.4,2.2-2.6l41.7-7c5.3-0.9,9.6-4.8,10.9-10c3.7-14.7,9.4-28.9,17.1-42 c2.7-4.6,2.4-10.3-0.7-14.6l-24.9-35c-0.7-1-0.6-2.5,0.3-3.4l29.8-29.8c0.7-0.7,1.4-0.8,1.9-0.8c0.6,0,1.1,0.2,1.5,0.5l34.5,24.6 c4.4,3.1,10.2,3.3,14.8,0.6c13-7.8,27.1-13.8,41.8-17.6c5.1-1.4,9-5.6,9.9-10.8l7.2-42.3c0.2-1.3,1.3-2.2,2.6-2.2h42.1 c1.3,0,2.4,0.9,2.6,2.2l7,41.7c0.9,5.3,4.8,9.6,10,10.9c15.1,3.8,29.5,9.7,42.9,17.6c4.6,2.7,10.3,2.5,14.7-0.6l34.5-24.8 c0.5-0.3,1-0.5,1.5-0.5c0.4,0,1.2,0.1,1.9,0.8l29.8,29.8c0.9,0.9,1,2.3,0.3,3.4l-24.7,34.7c-3.1,4.3-3.3,10.1-0.6,14.7 c7.8,13.1,13.6,27.2,17.4,41.9c1.3,5.2,5.6,9.1,10.8,9.9l42,7.1c1.3,0.2,2.2,1.3,2.2,2.6v42.1H451.9z"
                                      fill="#FF793F"
                                    />
                                    <path
                                      d="M239.4,136.001c-57,0-103.3,46.3-103.3,103.3s46.3,103.3,103.3,103.3s103.3-46.3,103.3-103.3S296.4,136.001,239.4,136.001 z M239.4,315.601c-42.1,0-76.3-34.2-76.3-76.3s34.2-76.3,76.3-76.3s76.3,34.2,76.3,76.3S281.5,315.601,239.4,315.601z"
                                      fill="#FF793F"
                                    />
                                  </svg>
                                </span>
                              </button>
                            </OverlayTrigger>
                          </div>
                        )}

                        {/* Profile dropdown for mobile */}
                        <div className="d-block d-md-none">
                          <UserAccountDropdown
                            dns={dns}
                            owlLinkProfile={owlLinkProfile}
                            handleChangeOwlLinkProfile={
                              handleChangeOwlLinkProfile
                            }
                          />
                        </div>
                      </div>
                    </div>

                    {/* Profile, socialLinks, links */}
                    <div className={styles.dashboard_left_scroll_box}>
                      {owlLinkProfile ? (
                        <div>
                          {/* Profile */}
                          <div className={styles.dashboard_left_title_box}>
                            {/* Image upload, name, bio */}
                            <div className={styles.dashboard_profile_box}>
                              {/* Image upload */}
                              {isProfileImgUploading ? (
                                <button
                                  className={styles.dashboard_profile_img_btn}
                                >
                                  {/* Show loading */}
                                  {isProfileImgUploading && (
                                    <Spinner
                                      animation="border"
                                      variant="secondary"
                                    />
                                  )}
                                </button>
                              ) : owlLinkProfile?.image ? (
                                // Profile image preview
                                <div style={{ position: "relative" }}>
                                  <button
                                    id="owl_link_profile_img_container"
                                    className={styles.dashboard_profile_img_btn}
                                    onClick={handleAddProfileClick}
                                    style={{
                                      border: "none",
                                      backgroundColor: owlLinkProfile.imageColor
                                        ? owlLinkProfile.imageColor
                                        : "#EEEEEF",
                                    }}
                                  >
                                    <img
                                      src={owlLinkProfile.image}
                                      alt="Profile picture"
                                      style={{
                                        height: "100%",
                                        width: "100%",
                                        objectFit: "cover",
                                      }}
                                    />
                                  </button>

                                  {/* Close icon */}
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="absolute pro-img-close-btn"
                                    style={{
                                      position: "absolute",
                                      right: "10px",
                                      cursor: "pointer",
                                    }}
                                    onClick={removeProfileImage}
                                  >
                                    <circle
                                      cx="12"
                                      cy="12"
                                      r="11"
                                      fill="#0D0C22"
                                      stroke="white"
                                      strokeWidth="2"
                                    />
                                    <g clipPath="url(#clip0)">
                                      <path
                                        d="M15.7766 8.21582L8.86487 15.1275"
                                        stroke="white"
                                        strokeWidth="2"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                      />
                                      <path
                                        d="M15.7823 15.1347L8.86487 8.21582"
                                        stroke="white"
                                        strokeWidth="2"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                      />
                                    </g>
                                    <defs>
                                      <clipPath id="clip0">
                                        <rect
                                          width="10.3784"
                                          height="10.3784"
                                          fill="white"
                                          transform="translate(7.13513 6.48633)"
                                        />
                                      </clipPath>
                                    </defs>
                                  </svg>
                                </div>
                              ) : (
                                // Upload new profile image
                                <button
                                  className={styles.dashboard_profile_img_btn}
                                  onClick={handleAddProfileClick}
                                  style={{ position: "relative" }}
                                >
                                  {/* Camera icon */}
                                  <svg
                                    style={{ width: "32px" }}
                                    viewBox="0 0 36 27"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      fillRule="evenodd"
                                      clipRule="evenodd"
                                      d="M2.98801 26.2729H29.8585C31.5073 26.2729 32.8465 24.9337 32.8465 23.2849V11.5848C32.8465 11.1888 32.5225 10.8648 32.1265 10.8648C31.7305 10.8648 31.4065 11.1888 31.4065 11.5848V23.2921C31.4065 24.1489 30.7153 24.8401 29.8585 24.8401H2.98801C2.13121 24.8329 1.44 24.1417 1.44 23.2921V7.07042C1.44 6.21362 2.13121 5.52242 2.98801 5.52242H9.03603C10.3464 5.52242 11.4984 4.68002 11.8872 3.42721L12.168 2.52721C12.3696 1.87921 12.9672 1.44 13.644 1.44H19.2097C19.8865 1.44 20.4841 1.87921 20.6857 2.52721L20.9665 3.42721C21.3553 4.68002 22.5073 5.52242 23.8177 5.52242H25.3513C25.7473 5.52242 26.0713 5.19842 26.0713 4.80242C26.0713 4.40641 25.7473 4.08241 25.3513 4.08241H23.8177C23.1409 4.08241 22.5433 3.64321 22.3417 2.99521L22.0609 2.09521C21.6721 0.842403 20.5201 0 19.2097 0H13.644C12.3336 0 11.1816 0.842403 10.7928 2.09521L10.512 2.99521C10.3104 3.64321 9.71283 4.08241 9.03603 4.08241H2.98801C1.3392 4.08241 0 5.42882 0 7.07042V23.2921C0 24.9337 1.3392 26.2729 2.98801 26.2729ZM9.09972 15.1776C9.09972 11.1384 12.3829 7.85522 16.4221 7.85522C20.4614 7.85522 23.7446 11.1384 23.7446 15.1776C23.7446 19.2169 20.4614 22.5001 16.4221 22.5001C12.3829 22.5001 9.09972 19.2169 9.09972 15.1776ZM10.5397 15.1848C10.5397 18.4249 13.1749 21.0673 16.4221 21.0673C19.6694 21.0673 22.3046 18.4249 22.3046 15.1848C22.3046 11.9448 19.6622 9.30242 16.4221 9.30242C13.1821 9.30242 10.5397 11.9448 10.5397 15.1848ZM12.6571 15.1777C12.6571 13.1041 14.3419 11.4121 16.4227 11.4121C18.5036 11.4121 20.1884 13.1041 20.1884 15.1777C20.1884 17.2513 18.4964 18.9433 16.4227 18.9433C14.3491 18.9433 12.6571 17.2513 12.6571 15.1777ZM14.0971 15.1849C14.0971 16.4665 15.1411 17.5105 16.4227 17.5105C17.7043 17.5105 18.7484 16.4665 18.7484 15.1849C18.7484 13.9033 17.7043 12.8593 16.4227 12.8593C15.1411 12.8593 14.0971 13.9033 14.0971 15.1849ZM32.8467 4.08243H35.2803C35.6763 4.08243 36.0003 4.40643 36.0003 4.80243C36.0003 5.19843 35.6763 5.52243 35.2803 5.52243H32.8467V7.95604C32.8467 8.35204 32.5227 8.67604 32.1267 8.67604C31.7307 8.67604 31.4067 8.35204 31.4067 7.95604V5.52243H28.9731C28.5771 5.52243 28.2531 5.19843 28.2531 4.80243C28.2531 4.40643 28.5771 4.08243 28.9731 4.08243H31.4067V1.64882C31.4067 1.25282 31.7307 0.928817 32.1267 0.928817C32.5227 0.928817 32.8467 1.25282 32.8467 1.64882V4.08243Z"
                                      fill="#FF793F"
                                    />
                                  </svg>

                                  {/* Size */}
                                  <div
                                    className={
                                      styles.dashboard_profile_img_size_text
                                    }
                                  >
                                    120 x 120px
                                  </div>
                                </button>
                              )}

                              {/* Input fields for */}
                              <div style={{ width: "100%" }}>
                                {/* Name */}
                                <input
                                  style={{ marginBottom: "14px" }}
                                  className="form-control owl_link_input_field"
                                  type="text"
                                  placeholder="Name"
                                  aria-label="Name"
                                  autoComplete="off"
                                  name="name"
                                  value={
                                    owlLinkProfile && owlLinkProfile.name
                                      ? owlLinkProfile.name
                                      : ""
                                  }
                                  onChange={handleChangeOwlLinkProfile}
                                />

                                {/* Bio */}
                                <textarea
                                  className="form-control owl_link_input_field"
                                  rows="2"
                                  placeholder="Description"
                                  aria-label="Description"
                                  autoComplete="off"
                                  name="description"
                                  value={
                                    owlLinkProfile && owlLinkProfile.description
                                      ? owlLinkProfile.description
                                      : ""
                                  }
                                  onChange={handleChangeOwlLinkProfile}
                                />
                              </div>

                              {/* Hidden image upload */}
                              <input
                                style={{ display: "none" }}
                                className="form-control owl_link_input_field"
                                type="file"
                                accept="image/*"
                                ref={hiddenAddProfileImageInput}
                                onChange={profileImageOnChange}
                              />
                            </div>
                          </div>

                          <hr style={{ margin: "10px 0" }} />

                          {/* CTA - NFT, Social, LINKS */}
                          <div className={styles.dashboard_left_title_box}>
                            <div className={styles.dashboard_left_cta_box}>
                              {/* Add nft link button */}
                              <button
                                className={styles.dashboard_left_cta_btn}
                                onClick={handleShowNftLinkPopup}
                              >
                                <svg
                                  width="14"
                                  height="14"
                                  viewBox="0 0 14 14"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M7 0C7.48325 0 7.875 0.391751 7.875 0.875V6.125H13.125C13.6082 6.125 14 6.51675 14 7C14 7.48325 13.6082 7.875 13.125 7.875H7.875V13.125C7.875 13.6082 7.48325 14 7 14C6.51675 14 6.125 13.6082 6.125 13.125V7.875H0.875C0.391751 7.875 0 7.48325 0 7C0 6.51675 0.391751 6.125 0.875 6.125H6.125V0.875C6.125 0.391751 6.51675 0 7 0Z"
                                    fill="#FF793F"
                                  />
                                </svg>
                                NFT
                              </button>

                              {/* Add social button */}
                              <button
                                className={styles.dashboard_left_cta_btn}
                                onClick={editSocialLinks}
                              >
                                <svg
                                  width="14"
                                  height="14"
                                  viewBox="0 0 14 14"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M7 0C7.48325 0 7.875 0.391751 7.875 0.875V6.125H13.125C13.6082 6.125 14 6.51675 14 7C14 7.48325 13.6082 7.875 13.125 7.875H7.875V13.125C7.875 13.6082 7.48325 14 7 14C6.51675 14 6.125 13.6082 6.125 13.125V7.875H0.875C0.391751 7.875 0 7.48325 0 7C0 6.51675 0.391751 6.125 0.875 6.125H6.125V0.875C6.125 0.391751 6.51675 0 7 0Z"
                                    fill="#FF793F"
                                  />
                                </svg>
                                Social
                              </button>

                              {/* Add link button */}
                              <button
                                className={styles.dashboard_left_cta_btn}
                                onClick={handleShowLinkPopup}
                              >
                                <svg
                                  width="14"
                                  height="14"
                                  viewBox="0 0 14 14"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M7 0C7.48325 0 7.875 0.391751 7.875 0.875V6.125H13.125C13.6082 6.125 14 6.51675 14 7C14 7.48325 13.6082 7.875 13.125 7.875H7.875V13.125C7.875 13.6082 7.48325 14 7 14C6.51675 14 6.125 13.6082 6.125 13.125V7.875H0.875C0.391751 7.875 0 7.48325 0 7C0 6.51675 0.391751 6.125 0.875 6.125H6.125V0.875C6.125 0.391751 6.51675 0 7 0Z"
                                    fill="#FF793F"
                                  />
                                </svg>
                                Link
                              </button>
                            </div>
                          </div>

                          <hr style={{ margin: "10px 0" }} />

                          {/* Socials */}
                          {owlLinkProfile &&
                            owlLinkProfile.socialLinks &&
                            Object.keys(owlLinkProfile.socialLinks).length >
                              0 && (
                              <div className={styles.dashboard_left_title_box}>
                                {/* Socials list box */}
                                <div
                                  className={styles.dashboard_left_social_box}
                                >
                                  <ul
                                    className={
                                      styles.dashboard_left_social_icons_list_box
                                    }
                                  >
                                    {Constants.SOCIAL_ICONS.map(
                                      (social, i) =>
                                        owlLinkProfile.socialLinks[
                                          social.type
                                        ] && (
                                          <li
                                            style={{ listStyle: "none" }}
                                            key={i}
                                            onClick={editSocialLinks}
                                          >
                                            <button
                                              className={
                                                styles.dashboard_left_social_icon_btn
                                              }
                                            >
                                              <img
                                                src={`images/social-icons-small/owl-link-social-icon-${social.type}.svg`}
                                              />
                                            </button>
                                          </li>
                                        )
                                    )}
                                  </ul>
                                </div>
                              </div>
                            )}

                          {/* List of links - Drag and drop */}
                          {owlLinkProfile && owlLinkProfile.links && (
                            <ReactSortable
                              className={styles.dashboard_links_list_box}
                              list={owlLinkProfile.links}
                              setList={(newState) => {
                                dragAndDropLinks(newState);
                              }}
                              handle={".handle"}
                            >
                              {
                                // List of links
                                owlLinkProfile.links.map((link, index) => (
                                  // Link
                                  <div
                                    className={styles.dashboard_links_list}
                                    key={index}
                                    onClick={() => {
                                      editLink(link, index);
                                    }}
                                  >
                                    {/* Draggable icon */}
                                    <div className="handle">
                                      <svg
                                        viewBox="0 0 5 8"
                                        className={styles.drag_drop_icon}
                                        xmlns="http://www.w3.org/2000/svg"
                                      >
                                        <path
                                          fillRule="evenodd"
                                          clipRule="evenodd"
                                          d="M1 2C1.55231 2 2 1.55231 2 1C2 0.447693 1.55231 0 1 0C0.447693 0 0 0.447693 0 1C0 1.55231 0.447693 2 1 2ZM4 2C4.55231 2 5 1.55231 5 1C5 0.447693 4.55231 0 4 0C3.44769 0 3 0.447693 3 1C3 1.55231 3.44769 2 4 2ZM2 4C2 4.55231 1.55231 5 1 5C0.447693 5 0 4.55231 0 4C0 3.44769 0.447693 3 1 3C1.55231 3 2 3.44769 2 4ZM4 5C4.55231 5 5 4.55231 5 4C5 3.44769 4.55231 3 4 3C3.44769 3 3 3.44769 3 4C3 4.55231 3.44769 5 4 5ZM2 7C2 7.55231 1.55231 8 1 8C0.447693 8 0 7.55231 0 7C0 6.44769 0.447693 6 1 6C1.55231 6 2 6.44769 2 7ZM4 8C4.55231 8 5 7.55231 5 7C5 6.44769 4.55231 6 4 6C3.44769 6 3 6.44769 3 7C3 7.55231 3.44769 8 4 8Z"
                                        />
                                      </svg>
                                    </div>

                                    {/* Image */}
                                    {link && link.img && (
                                      <img
                                        src={link.img}
                                        alt="Link icon"
                                        className={
                                          styles.dashboard_links_list_img
                                        }
                                      />
                                    )}
                                    {/* Title and link */}
                                    <div style={{ width: "100%" }}>
                                      {/* Title */}
                                      <div
                                        className={
                                          styles.dashboard_links_list_title
                                        }
                                      >
                                        {link.title}

                                        {/* Link */}
                                        {link.url && (
                                          <span
                                            className={
                                              styles.dashboard_links_list_link
                                            }
                                          >
                                            {" - " + link.url}
                                          </span>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                ))
                              }
                            </ReactSortable>
                          )}
                        </div>
                      ) : (
                        // Loading
                        <div>
                          {/* Profile */}
                          <div className={styles.dashboard_left_title_box}>
                            {/* Image upload, name, description */}
                            <div className={styles.dashboard_profile_box}>
                              {/* Image upload */}
                              <button
                                className={styles.dashboard_profile_img_btn}
                                style={{
                                  backgroundColor: "#EEEEEF",
                                  borderColor: "#EEEEEF",
                                }}
                              />

                              {/* Input fields for */}
                              <div style={{ width: "100%" }}>
                                {/* Name */}
                                <div
                                  style={{
                                    backgroundColor: "#EEEEEF",
                                    borderColor: "#EEEEEF",
                                    height: "42px",
                                    marginBottom: "20px",
                                  }}
                                  className="form-control owl_link_input_field"
                                />

                                {/* Bio */}
                                <div
                                  style={{
                                    backgroundColor: "#EEEEEF",
                                    borderColor: "#EEEEEF",
                                    height: "42px",
                                  }}
                                  className="form-control owl_link_input_field"
                                />
                              </div>
                            </div>
                          </div>

                          <hr style={{ margin: "10px 0" }} />

                          {/* CTA - NFT, Social, LINKS */}
                          <div className={styles.dashboard_left_title_box}>
                            <div className={styles.dashboard_left_cta_box}>
                              {/* Add nft link button */}
                              <button
                                className={styles.dashboard_left_cta_btn}
                                style={{
                                  backgroundColor: "#EEEEEF",
                                  boxShadow: "none",
                                  border: "none",
                                  padding: "0px",
                                  width: "84px",
                                }}
                              />

                              {/* Add social button */}
                              <button
                                className={styles.dashboard_left_cta_btn}
                                style={{
                                  backgroundColor: "#EEEEEF",
                                  boxShadow: "none",
                                  border: "none",
                                  padding: "0px",
                                  width: "84px",
                                }}
                              />

                              {/* Add link button */}
                              <button
                                className={styles.dashboard_left_cta_btn}
                                style={{
                                  backgroundColor: "#EEEEEF",
                                  boxShadow: "none",
                                  border: "none",
                                  padding: "0px",
                                  width: "84px",
                                }}
                              />
                            </div>
                          </div>

                          <hr style={{ margin: "10px 0" }} />

                          {/* Socials */}
                          <div className={styles.dashboard_left_title_box}>
                            {/* Socials list box */}
                            <div
                              className={styles.dashboard_left_social_box}
                              style={{ columnGap: "12px" }}
                            >
                              {/* Add social button */}
                              <button
                                style={{
                                  backgroundColor: "#EEEEEF",
                                  boxShadow: "none",
                                  border: "none",
                                  padding: "0px",
                                  width: "44px",
                                  height: "44px",
                                  borderRadius: "50%",
                                }}
                              />
                              <button
                                style={{
                                  backgroundColor: "#EEEEEF",
                                  boxShadow: "none",
                                  border: "none",
                                  padding: "0px",
                                  width: "44px",
                                  height: "44px",
                                  borderRadius: "50%",
                                }}
                              />
                              <button
                                style={{
                                  backgroundColor: "#EEEEEF",
                                  boxShadow: "none",
                                  border: "none",
                                  padding: "0px",
                                  width: "44px",
                                  height: "44px",
                                  borderRadius: "50%",
                                }}
                              />
                            </div>
                          </div>

                          {/* Links */}
                          <div className={styles.dashboard_left_title_box}>
                            {/* List of links */}
                            <div
                              style={{
                                backgroundColor: "#EEEEEF",
                                borderColor: "#EEEEEF",
                                height: "48px",
                                marginBottom: "16px",
                              }}
                              className="form-control owl_link_input_field"
                            />
                            <div
                              style={{
                                backgroundColor: "#EEEEEF",
                                borderColor: "#EEEEEF",
                                height: "48px",
                                marginBottom: "16px",
                              }}
                              className="form-control owl_link_input_field"
                            />
                          </div>
                        </div>
                      )}
                    </div>

                    {/* Save changes button */}
                    <div className={styles.dashboard_save_changes_btn_box}>
                      <div className="row justify-content-end">
                        {/* Save changes */}
                        <div className="col-3">
                          <button
                            className="btn owl_link_dashboard_filled_button"
                            style={{ width: "100%" }}
                            onClick={saveOwlLinkProfile}
                            disabled={
                              isSaving ||
                              _.isEqual(owlLinkProfile, previousOwlLinkProfile)
                            }
                          >
                            Save
                          </button>
                        </div>

                        {/* Mint to publish */}
                        {mintOwlLinkProfileFlag && (
                          <div className="col-3">
                            <OverlayTrigger
                              placement="auto"
                              overlay={
                                <Tooltip>
                                  There is a one-time fee of 10 STX to register
                                  to your Owl Link. Updates to your profile are
                                  free. No further charges. You can continue to
                                  update your profile before and after
                                  publishing/registering on the chain without
                                  any issues.
                                </Tooltip>
                              }
                            >
                              <button
                                className="btn owl_link_dashboard_filled_button"
                                style={{ width: "100%" }}
                                onClick={() => {
                                  mintOwlLinkProfile(dns, mintCallbackFunction);
                                }}
                                disabled={isSaving || isLastTxPending}
                              >
                                Publish
                              </button>
                            </OverlayTrigger>
                          </div>
                        )}

                        {/* Republish gaia url */}
                        {republishOwlLinkProfileFlag && (
                          <div className="col-3">
                            <OverlayTrigger
                              placement="auto"
                              overlay={
                                <Tooltip>
                                  There is a one-time fee of 10 STX to register
                                  to your Owl Link. Updates to your profile are
                                  free. No further charges. You can continue to
                                  update your profile before and after
                                  publishing/registering on the chain without
                                  any issues.
                                </Tooltip>
                              }
                            >
                              <button
                                className="btn owl_link_dashboard_filled_button"
                                style={{ width: "100%" }}
                                onClick={() => {
                                  republishOwlLinkProfile(
                                    dns,
                                    mintCallbackFunction
                                  );
                                }}
                                disabled={isSaving || isLastTxPending}
                              >
                                Republish
                              </button>
                            </OverlayTrigger>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>

                  {/* Right side */}
                  <div className={styles.dashboard_right_box}>
                    {/* Nav bar */}
                    <div className="container-fluid">
                      <div className="row">
                        <div className="col-md-12">
                          {/* Account dropdown */}
                          <div className={styles.dashboard_right_nav_box}>
                            <UserAccountDropdown
                              dns={dns}
                              owlLinkProfile={owlLinkProfile}
                              handleChangeOwlLinkProfile={
                                handleChangeOwlLinkProfile
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    {/* Preview */}
                    <div
                      style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "center",
                        overflow: "auto",
                      }}
                    >
                      <div className={styles.dashboard_right_preview_box}>
                        <ProfilePreview
                          owlLinkProfile={owlLinkProfile}
                          dns={dns}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Get Btc domain */}
          {showBTCDomainPopup && <GetBtcDomain />}
        </section>

        {/* Links popup */}
        <Modal
          show={showLinkPopup}
          onHide={handleCloseLinkPopup}
          backdrop="static"
          keyboard={false}
          centered
          size="md"
        >
          {/* Header */}
          <div className={styles.dashboard_modal_header_box}>
            <div>{link && link.id ? "Edit" : "Add"} link</div>
            <button
              className={styles.dashboard_modal_close_icon_btn_box}
              onClick={handleCloseLinkPopup}
            >
              <svg
                width="10"
                height="10"
                viewBox="0 0 10 10"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M0.898377 0.898804C1.2108 0.586385 1.71733 0.586385 2.02975 0.898804L4.9996 3.86865L7.96945 0.898804C8.28186 0.586385 8.7884 0.586385 9.10082 0.898804C9.41324 1.21122 9.41324 1.71776 9.10082 2.03018L6.13097 5.00002L9.10082 7.96987C9.41324 8.28229 9.41324 8.78882 9.10082 9.10124C8.7884 9.41366 8.28186 9.41366 7.96945 9.10124L4.9996 6.13139L2.02975 9.10124C1.71733 9.41366 1.2108 9.41366 0.898377 9.10124C0.585958 8.78882 0.585958 8.28229 0.898377 7.96987L3.86823 5.00002L0.898377 2.03018C0.585958 1.71776 0.585958 1.21122 0.898377 0.898804Z"
                  fill="#FF793F"
                />
              </svg>
            </button>
          </div>

          {/* Body */}
          <div className={styles.dashboard_modal_body_box}>
            {/* Select type of link dropdown */}
            {/* Title */}
            <input
              className="form-control owl_link_input_field"
              type="text"
              placeholder="Title"
              aria-label="Link Title"
              autoComplete="off"
              name="title"
              value={link && link.title ? link.title : ""}
              onChange={(e) => {
                handleChangeLink(false, e);
              }}
            />

            {/* URL */}
            <input
              style={{ marginTop: "14px" }}
              className="form-control owl_link_input_field"
              type="text"
              placeholder="URL"
              aria-label="Link URL"
              autoComplete="off"
              name="url"
              value={link && link.url ? link.url : ""}
              onChange={(e) => {
                handleChangeLink(false, e);
              }}
            />

            {/* Embed */}
            <div style={{ marginTop: "14px" }} className="form-check">
              <input
                className="form-check-input owl_link_checkbox_field"
                type="checkbox"
                id="embed_checkbox_id"
                name="embed"
                value={link && link.embed ? link.embed : ""}
                checked={link && link.embed ? link.embed : false}
                onChange={(e) => {
                  handleChangeLink(true, e);
                }}
              />
              <label className="form-check-label" htmlFor="embed_checkbox_id">
                Embed
              </label>
            </div>

            {/* Error message */}
            {errorMessage && (
              <div
                style={{ fontSize: "14px", color: "red", marginTop: "14px" }}
              >
                {errorMessage}
              </div>
            )}
          </div>

          {/* Footer */}
          <div className={styles.dashboard_modal_footer_box}>
            {/* Left align */}
            <div>
              {/* Delete link */}
              {link && link.id && (
                <div>
                  <button
                    className="btn owl_link_dashboard_outline_button"
                    style={{ fontSize: "16px" }}
                    onClick={deleteLink}
                    disabled={isLinkImgUploading}
                  >
                    Delete
                  </button>
                </div>
              )}
            </div>

            {/* Right align */}
            <div>
              {/* Add new link */}
              {link && !link.id && (
                <div style={{ display: "flex", columnGap: "10px" }}>
                  <button
                    className="btn owl_link_dashboard_outline_button"
                    style={{ fontSize: "16px" }}
                    onClick={handleCloseLinkPopup}
                    disabled={isLinkImgUploading}
                  >
                    Cancel
                  </button>
                  <button
                    className="btn owl_link_dashboard_filled_button"
                    style={{ fontSize: "16px" }}
                    onClick={addLink}
                    disabled={
                      !link ||
                      !link.title ||
                      !link.url ||
                      isLinkImgUploading ||
                      isLinkImgUploading
                    }
                  >
                    Add
                  </button>
                </div>
              )}

              {/* Edit link */}
              {link?.id && (
                <div style={{ display: "flex", columnGap: "10px" }}>
                  <button
                    className="btn owl_link_dashboard_outline_button"
                    style={{ fontSize: "16px" }}
                    onClick={handleCloseLinkPopup}
                    disabled={isLinkImgUploading}
                  >
                    Cancel
                  </button>
                  <button
                    className="btn owl_link_dashboard_filled_button"
                    style={{ fontSize: "16px" }}
                    onClick={saveLink}
                    disabled={!link.title || !link.url || isLinkImgUploading}
                  >
                    Update
                  </button>
                </div>
              )}
            </div>
          </div>
        </Modal>

        {/* Social link popup */}
        <Modal
          show={showSocialLinkPopup}
          onHide={handleCloseSocialLinkPopup}
          backdrop="static"
          keyboard={false}
          centered
          size="md"
        >
          {/* Header */}
          <div className={styles.dashboard_modal_header_box}>
            <div>Socials</div>
            <button
              className={styles.dashboard_modal_close_icon_btn_box}
              onClick={handleCloseSocialLinkPopup}
            >
              <svg
                width="10"
                height="10"
                viewBox="0 0 10 10"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M0.898377 0.898804C1.2108 0.586385 1.71733 0.586385 2.02975 0.898804L4.9996 3.86865L7.96945 0.898804C8.28186 0.586385 8.7884 0.586385 9.10082 0.898804C9.41324 1.21122 9.41324 1.71776 9.10082 2.03018L6.13097 5.00002L9.10082 7.96987C9.41324 8.28229 9.41324 8.78882 9.10082 9.10124C8.7884 9.41366 8.28186 9.41366 7.96945 9.10124L4.9996 6.13139L2.02975 9.10124C1.71733 9.41366 1.2108 9.41366 0.898377 9.10124C0.585958 8.78882 0.585958 8.28229 0.898377 7.96987L3.86823 5.00002L0.898377 2.03018C0.585958 1.71776 0.585958 1.21122 0.898377 0.898804Z"
                  fill="#FF793F"
                />
              </svg>
            </button>
          </div>

          {/* Body */}
          <div
            className={styles.dashboard_modal_body_box}
            style={{ paddingBottom: "0px" }}
          >
            {/* List of social fields */}
            {Constants.SOCIAL_ICONS.map((eachSocialObj, index) => (
              <div key={index} style={{ position: "relative" }}>
                <input
                  style={{ marginBottom: "14px" }}
                  className="form-control owl_link_input_field owl_link_social_input_field"
                  type="text"
                  placeholder={eachSocialObj.placeholder}
                  aria-label={eachSocialObj.placeholder}
                  autoComplete="off"
                  name={eachSocialObj.type}
                  value={
                    socialLinks && socialLinks[eachSocialObj.type]
                      ? socialLinks[eachSocialObj.type]
                      : ""
                  }
                  onChange={handleChangeSocialLink}
                />

                <span className={styles.dashboard_social_input_icon}>
                  <img
                    src={`data:image/svg+xml;utf8,${encodeURIComponent(
                      Constants.SOCIAl_ICONS_SVG[eachSocialObj.type]
                    )}`}
                  />
                </span>
              </div>
            ))}

            {/* Error message */}
            {errorMessage && (
              <div
                style={{ fontSize: "14px", color: "red", marginTop: "14px" }}
              >
                {errorMessage}
              </div>
            )}
          </div>

          {/* Footer */}
          <div className={styles.dashboard_modal_footer_box}>
            {/* Left align */}
            <div />

            {/* Right align */}
            <div>
              <div style={{ display: "flex", columnGap: "10px" }}>
                <button
                  className="btn owl_link_dashboard_outline_button"
                  style={{ fontSize: "16px" }}
                  onClick={handleCloseSocialLinkPopup}
                >
                  Cancel
                </button>
                <button
                  className="btn owl_link_dashboard_filled_button"
                  style={{ fontSize: "16px" }}
                  onClick={saveSocialLinks}
                >
                  Add
                </button>
              </div>
            </div>
          </div>
        </Modal>

        {/* Confirmation popup */}
        <Modal
          show={showTransactionConfirmationPopup}
          onHide={handleCloseTransactionConfirmationPopup}
          backdrop="static"
          keyboard={false}
          centered
          size="md"
        >
          {/* Header */}
          <div className={styles.dashboard_modal_header_box}>
            <div />
            <button
              className={styles.dashboard_modal_close_icon_btn_box}
              onClick={handleCloseTransactionConfirmationPopup}
            >
              <svg
                width="10"
                height="10"
                viewBox="0 0 10 10"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M0.898377 0.898804C1.2108 0.586385 1.71733 0.586385 2.02975 0.898804L4.9996 3.86865L7.96945 0.898804C8.28186 0.586385 8.7884 0.586385 9.10082 0.898804C9.41324 1.21122 9.41324 1.71776 9.10082 2.03018L6.13097 5.00002L9.10082 7.96987C9.41324 8.28229 9.41324 8.78882 9.10082 9.10124C8.7884 9.41366 8.28186 9.41366 7.96945 9.10124L4.9996 6.13139L2.02975 9.10124C1.71733 9.41366 1.2108 9.41366 0.898377 9.10124C0.585958 8.78882 0.585958 8.28229 0.898377 7.96987L3.86823 5.00002L0.898377 2.03018C0.585958 1.71776 0.585958 1.21122 0.898377 0.898804Z"
                  fill="#FF793F"
                />
              </svg>
            </button>
          </div>

          {/* Body */}
          <div
            className={styles.dashboard_modal_body_box}
            style={{ paddingBottom: "0px" }}
          >
            <div className={styles.dashboard_trans_confirm_popup_box}>
              {/* Image */}
              <img
                className={styles.dashboard_trans_confirm_img}
                src="images/public/owl-link-transaction-confirmation.svg"
              />

              {/* Text content */}
              <h1 className={styles.dashboard_trans_confirm_title}>
                Hey! Your {dns} Owl Link <br className="d-none d-md-block" />
                is on chain now.
              </h1>

              {/* Description */}
              <p className={styles.dashboard_trans_confirm_description}>
                {"Check your transaction status on explorer "}
                <a
                  style={{ textDecoration: "underline", color: "#ff793f" }}
                  href={formStacksExplorerUrl(transactionId)}
                  target="_blank"
                  rel="noreferrer"
                >
                  {"here "}
                  <svg
                    width="10"
                    height="10"
                    viewBox="0 0 12 12"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fillRule="evenodd"
                      clipRule="evenodd"
                      d="M3.5044 0.743397C3.5044 0.33283 3.83723 -6.71395e-08 4.2478 0L11.2566 6.60206e-07C11.6672 6.60206e-07 12 0.33283 12 0.743397L12 7.7522C12 8.16277 11.6672 8.4956 11.2566 8.4956C10.846 8.4956 10.5132 8.16277 10.5132 7.7522V2.53811L1.26906 11.7823C0.978742 12.0726 0.50805 12.0726 0.217736 11.7823C-0.0725787 11.4919 -0.0725784 11.0213 0.217736 10.7309L9.46189 1.48679L4.2478 1.48679C3.83723 1.48679 3.5044 1.15396 3.5044 0.743397Z"
                      fill="#ff793f"
                    />
                  </svg>
                </a>
              </p>

              {/* Owl link logo */}
              <a className={styles.popup_owl_link_logo_box}>
                <svg
                  className={styles.popup_owl_link_logo_icon}
                  viewBox="0 0 34 47"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M28.4399 46.0283H25.5764C11.4508 46.0283 0 34.5775 0 20.4519V14.8314C0 6.64035 6.64035 0 14.8313 0H19.1682C27.3592 0 33.9996 6.64035 33.9996 14.8314V21.0058C33.9996 21.3919 33.8523 21.7627 33.5884 22.045L33.5355 22.1016C33.3616 22.2877 33.1975 22.4635 33.1598 22.4802C32.3007 22.8258 31.9838 23.0366 31.3124 23.4832C31.2484 23.5258 31.1812 23.5705 31.11 23.6177C27.126 26.2668 24.6661 30.7316 24.6661 35.6668C24.6661 38.3467 25.4127 40.9192 26.7476 43.1388C27.28 44.023 27.9065 44.85 28.6173 45.6044C28.7692 45.7655 28.6608 46.0283 28.4399 46.0283ZM9.52275 7.91687C6.59739 8.12144 4.17835 10.5763 4.01981 13.5068C3.84592 16.7748 6.35702 19.5825 9.61992 19.7615C9.67106 19.7666 9.72221 19.7717 9.76823 19.782H13.2664C14.3404 19.782 15.3479 20.2013 16.1099 20.9582L16.9998 21.8481L17.8897 20.9582C18.6517 20.2013 19.6592 19.782 20.7332 19.782H24.5996C24.6814 19.7513 24.7632 19.7308 24.8502 19.7206C27.9545 19.3012 30.2048 16.5139 29.9696 13.3789C29.7496 10.4689 27.3869 8.12144 24.4768 7.91687C21.2855 7.69185 18.4983 10.0035 18.1505 13.1794C18.084 13.7676 17.5879 14.2074 16.9998 14.2074C16.4117 14.2074 15.9156 13.7676 15.8491 13.1794C15.5013 10.0086 12.7192 7.70208 9.52275 7.91687ZM31.112 27.2969C31.8914 26.5512 32.7797 25.9073 33.7637 25.3938C33.8701 25.3381 34 25.4179 34 25.5375V46.0289H33.8098C33.7213 45.9434 33.6251 45.8642 33.5126 45.8018C32.2652 45.1078 31.1836 44.199 30.2917 43.1413C28.5543 41.0808 27.5519 38.4459 27.5519 35.6668C27.5519 32.4259 28.8755 29.4367 31.112 27.2969ZM10.0633 16.2915C11.4163 16.2915 12.5131 15.1947 12.5131 13.8417C12.5131 12.4888 11.4163 11.392 10.0633 11.392C8.71038 11.392 7.6136 12.4888 7.6136 13.8417C7.6136 15.1947 8.71038 16.2915 10.0633 16.2915ZM26.3853 13.8417C26.3853 15.1947 25.2886 16.2915 23.9356 16.2915C22.5827 16.2915 21.4859 15.1947 21.4859 13.8417C21.4859 12.4888 22.5827 11.392 23.9356 11.392C25.2886 11.392 26.3853 12.4888 26.3853 13.8417Z"
                  />
                </svg>
              </a>
            </div>
          </div>
        </Modal>

        {/* Nft link popup */}
        <NftPopup
          link={link}
          setLink={setLink}
          addLink={addLink}
          saveLink={saveLink}
          deleteLink={deleteLink}
          showNftLinkPopup={showNftLinkPopupFlag}
          setShowNftLinkPopupFlag={setShowNftLinkPopupFlag}
          nftsArray={nftsArray}
          nftsTotal={nftsTotal}
          nftsPaginationActivePage={nftsPaginationActivePage}
          setNftsPaginationActivePage={setNftsPaginationActivePage}
          getNftsImageObjFromGaia={getNftsImageObjFromGaia}
        />

        {/* Settings popup */}
        <SettingsPopup
          showSettingsPopupFlag={showSettingsPopupFlag}
          setShowSettingsPopupFlag={setShowSettingsPopupFlag}
          owlLinkProfile={owlLinkProfile}
          dns={dns}
          owlLinkSettings={owlLinkSettings}
          setOwlLinkSettings={setOwlLinkSettings}
          previousOwlLinkSettings={previousOwlLinkSettings}
          setPreviousOwlLinkSettings={setPreviousOwlLinkSettings}
          handleChangeOwlLinkProfile={handleChangeOwlLinkProfile}
          saveOwlLinkProfile={saveOwlLinkProfile}
        />
      </main>
    </>
  );
}
