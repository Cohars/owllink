# Owl Link

The only bio link that stays forever.

## FAQ

1. What is a bio link?
It is a clickable URL link that you can add to your bio page on social media. Today, most social media platforms have a bio page, but they limit you to adding just a link. It becomes difficult to show your entire portfolio of products, socials, and links. This is where bio links help. Today, almost everyone familiar with Instagram uses the "link in bio" phrasing. Bio links help you in creating them. Share your links on YouTube, Instagram, Twitter, and more content with just one bio link.
What can I do with the link in my bio?

2. How does Owl Link work?
Your bio link should always point to your most important links. Add your portfolio links, other social profiles, email ids, your NFT collection, and digital assets.

3. Do I need a .btc domain to create Owl Link?
Owl Links are decentralized bio links from [btc.us ↗](https://btc.us/). When you publish an Owl Link, it is stored in the Stacks blockchain which is secured by Bitcoin. All your data is stored on a decentralized private storage called GAIA, which only you can access. Not anyone, not Stacks, not Owl Link.

4. What wallet does Owl Link support?
Yes, Owl Link requires a .btc domain to get started with. If you don't have one, you need to register a .btc domain from [btc.us ↗](https://btc.us/) before creating Owl Link.
Owl Link supports [Hiro Wallet ↗](https://www.hiro.so/wallet). Please install from app stores of Brave, Chrome, Firefox to get started.

5. What is the cost of creating Owl Link?
It costs 10 STX / 29 USD to publish / register Owl Link on-chain. It is a one-time fee. If you don't have Stacks? [Pay with Stripe ↗](https://buy.stripe.com/00g039eWq0NTcJq146) to get started.

6. Why should I use Owl Link over linktr.ee, bio.link, and other bio link apps?
A decentralized bio link using Owl Link gives you full ownership and control over your identity and data. You can also showcase your digital assets like names, NFTs, collectibles with verifiable ownership. There are no trackers, no ads, no censorship. It is privacy-focused.

7. Can I use my NFTs, Collectibles in Owl Link?
Yes, you can add it to Owl Link.

8. How many links can I add to Owl Link?
Add as many links as you wish and change them as often as possible.

9. How can I share my Owl Link on social media platforms?
Just add the Owl Link to your profile page. It could be on the website link or product link.

10. Who is the team behind Owl Link?
We are developers from Team [BlockSurvey ↗](https://blocksurvey.io/?ref=owl-link).

## Schema.org

Owl Link profile data structure is formed as per the [Scheme.org Person](https://schema.org/Person) standard. Owl.link profile properties are name, description, image, email, url and sameAs.

## API for Owl Link

Owl Link has a public API to fetch profile JSON. Here is the API link, https://owl.link/api/[yourName] <br />
Ex, https://owl.link/api/owllink.btc

## Getting Started

Owl.link is built using Next.js and interacts with Stacks Blockchain.

Run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## License

[MIT License](LICENSE)
